Bitbucket for Validation & Samples of SNOMED2HL7 project

it contains

* Samples: this directory contains examples figures of the normalization and binding process of different SNOMED concepts to HL7 RIM.
* Validation: this directory contains a csv file with the validation of the tool at the EURECA project. This validation was performed on 24 datasets with 20,240 patients, 2,383 vocabulary concepts (1,206 unique concepts) and with over 700,000 instances.